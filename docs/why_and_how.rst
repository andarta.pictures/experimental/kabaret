===========
Why and How
===========


Why another Pipeline software ?
===============================

There are many existing solutions, both commercial/closed and free/open, to handle the task of "CG Project Management". 
Notable and recent examples include `CGWire <https://www.cg-wire.com/>`_, `Kurtis <http://texels.com/>`_, `shotgun <https://www.shotgunsoftware.com/>`_...

Almost all of them fall in two categories: Meta-Data management or Dataflow Modeling.

The **Meta-Data** Management tools are often *Production Team* oriented and subtitled as "*Better than google docs™*".
They manage a more or less flexible "entity" system and their dependencies: assets info, shot list, statuses, frame ranges, etc.

As vital as this is to complete a CG project successfully, it does not give any help in the practical matter: the technical side of an artistic cooperative work.

The **Dataflow** Modeling tools are often *Talent Team* oriented and captioned as "*Automate everything !©*".
There is a strong culture of dataflow in the CG world because many of our Digital Content Creation tool use them under the hood, with great success.
Automating non-artistic tasks involve things like dependencies, parameters and process execution. It sounds pretty much like a dataflow.

As efficient as they are to manage 3D data or image manipulations, dataflows do come with restrictions. A major one being that the graph needs to be "acyclic".
This becomes a real issue when you try to represent the highly iterative day-to-day tasks of an artistic cooperative work.

After years of using and implementing different flavors and mixes of both of those, it is now obvious that the solution is elsewhere.

Hence the need for another approach.


How is Kabaret different ?
=============================

Interestingly, the CG world is not flooded by `BPM <https://en.wikipedia.org/wiki/Business_process_management>`_ and `Workflow <https://en.wikipedia.org/wiki/Workflow>`_ concepts, despite the fact that the BPM `definition <https://bpm.com/what-is-bpm>`_ pretty much describes what we are looking for:

	“*Business Process Management (BPM) is a discipline involving any combination of modeling, automation, execution, control, measurement and optimization of business activity flows, in support of enterprise goals, spanning systems, employees, customers and partners within and beyond the enterprise boundaries.*”

The reason might probably be that the "Artistic" world is not keen on being treated as an "Industrial Business" or an "Orchestrated and repeatable pattern of business activity".
It is nevertheless what project management aims to bring to the table.

**Workflow** does a pretty good job as modeling the "*highly iterative day-to-day tasks of an artistic cooperative work*" and **is a better fit than Dataflow**.
On the other side it does not deal down to data processing **and does not replace the Dataflow**.

Representing both in a single graph is the idea that led to **Kabaret**.

But there's more !

We also wanted to provide:

	* **A framework, not a Solution**
	Every need is different and every project should not deal with decisions made for another project or studio.
	Any choice you did not make yourself is not the better one.
	Kabaret gives you an abstract set of tools that you can use as you want.
	The balance of Workflow / Dataflow you need is up to you.

	* **Rapid Prototyping, Fast Development, Live Update, Schema-less**
	This is the only path to happy end-users.
	Having 100 Artists working on the project for six months should not mean that the workflow
	can't evolve, and it should not require downtime or migrations to do so.
	
	* **The end of GUI development**
	It can cost more than the implementation of the actual pipeline features.
	We need automatic default GUI, with configurable behavior.
	Of course you can extend or even replace the default,
	but you'll get a pretty good GUI out of the box.

	* **Problem isolation, Reusability of solutions**
	Two projects are not the same, but they surely share a lot:
	Naming conventions, version control, long-running tasks dispatching, etc.
	Once something is dealt with, it is available for every other project.
	Once a solution is in use, updating it updates all projects.

	* **Modular and Extendable**
	There will always be more.
	Let's deal with that later by adding blocks :D


Doesn't it whet your appetite ? :D


What Kabaret is not ?
=====================

Kabaret is not a Pipeline software, nor an exhaustive Pipeline solution.
It is a Framework and it delivers only generic features that may or may not be used by someone to build his very own solution.

That being said, there are a number of generic features that are not available in Kabaret.
The reason is that we want to keep it to the bare minimum so that code quality prevails over feature quantity.
It does not mean that we won't provide those, on the contrary. We focused on delivering an extensible architecture so that whatever would be the scope of a missing feature, one can implement it without modifying Kabaret's code, and package the result to share it with the community.


Here are some examples of what we will provide as 'extensions' packages:
	
	* A Script view, with python syntax highlighting and code completion.
	* A collection of Flow Objects to handle planning information along with a Gantt view to visualize and edit them.
	* Other collections of Flow Objects like BPM Workflow, Shotgun sync, mail automation, etc.
	* An Actor to manage subprocess spawned by the flow.
	* An Actor to manage users, teams and their preferences.
	* Some Actors as alternative key-value stores.

You can look for extensions on the `Python Package Index <https://pypi.org/search/?q=kabaret>`_ or discuss with the community on the `Kabaret Studio <https://discord.gg/NmJDHsN>`_ discord channel.

We encourage you to share your extensions there too :)
