.. _credits:


*******
Credits
*******

=======
Authors
=======

First versions of Kabaret were conceived and implemented between 2012 and 2015 at SupamonkS Studio, Paris.

The primary authors are (and/or have been):

	* Damien 'Dee' Coureau
	* Sebastien 'Zwib' Ho
	* Valerian Prevost
	* Ivans Saponenko
	* Steeve 'Firegreen' Vincent

We accumulate experience as Artists, Technical Directors, Developer and Production Director on
hundreds of commercial spots and commercial series, as well as on VFX and Full CG features movies like
`Blueberry <http://www.imdb.com/rg/em_share/title_web/title/tt0276830/>`_, 
`Splice <http://www.imdb.com/rg/em_share/title_web/title/tt1017460/>`_, 
`Irreversible <http://www.imdb.com/rg/em_share/title_web/title/tt0290673/>`_, 
`Despicable Me <http://www.imdb.com/rg/em_share/title_web/title/tt1323594/>`_,
`The Lorax <http://www.imdb.com/rg/em_share/title_web/title/tt1482459/>`_...

We have a deep faith in the open source philosophy and we wish every CG Talent could focus on the beauty of 
their work (may it be cost tracking, pixel enhancement, or code magnificence) instead of struggling 
with the machine.
**Kabaret** is our contributions to make this dream get real.

We hope you'll join us in this adventure.

=======
Mentors
=======

Many ideas in Kabaret come from the outstanding people we had the chance to meet or work with.

Most notably:

	* Etienne 'Chex' Pecheux, on the dataflow and automation.
	* Albert 'Lobo' Bonnefous, on the overall CG world. 
	* Pierrick 'Ick' Brault, on pipeline and asset exploitation.
	* Thierry 'Mamouth' Lauthelier, for ignition.
	* Alexis Casas, for understanding and support.
	* Nicolas 'Nikko' Brack, for endless higher expectations :)
	
============
Contributors
============

We welcome patches, bug reports and support.
If you think your name should appears here, please contact us on the  `Kabaret Studio <https://discord.gg/NmJDHsN>`_ discord channel.

