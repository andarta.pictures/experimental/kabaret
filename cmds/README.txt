==============================
Sublime Text 3.0 - Build Tools
==============================

Sublime Text 3.0 does not handle variable in build cmd, so we use sublime_py[2/3]_launcher.[bat/sh]
This launchers use environment variables.

Some variables must be define in your environment:
	
	- KBR_PYTHON_2 	: path the to the python 2.7+ to use.
		It must have all dependencies installed 
		+ one of the Qt wrappers (PyQt4, PyQt5, PySide, PySide2)
	
	- KBR_PYTHON_3 	: path the to the python 3.6+ to use.
		It must have all dependencies installed
		+ one of the Qt wrappers (PyQt4, PyQt5, PySide, PySide2)
		+ sphinx, sphinx-autobuild and sphinx_rtd_theme if you want to edit/build the doc
		+ sphinx-pypi-upload if you want to updload the doc (you need PyPI access for that...)
		+ setuptools, wheel if you want to package
		+ setuptools if you want to upload to pypi (you need PyPI access for that...)

	- KBR_SESSION_FLAGS	: the flags to use. Something like: 
		"--host <redis-host> --port <redis-port> --cluster <cluster_name> --session <session_name>

Some are provided by the build_systems defined in kabaret.sublime-project:
	
	- KBR_MODULE 	: the module to use (gui, cli, etc...)

The sublime_kabaret.[bat/sh] files are example of the commands needed to run Sublime Text as IDE to dev on kabaret.
Copy the ones you need and edit it to your needs.
Run it and go to "Project > Open Project" and select the kabaret.sublime-project at the root of this repository.

- This sucks, I know :/ -
But that's the best I can come up with right now, please give any helpfull suggestions you might have ! :)
